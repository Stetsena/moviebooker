# Movie Booker
Implemented and ready for use

### Prerequisites

* Please make sure you have latest SBT installed on your environment!

* Please Download and run latest MongoDB and set up your mongoDB connection info in
> resources/application.conf


### Implementation Details

- Solution is using mongodb because of atomicity of update operations,
  That means we can run multiple services updating same document and no conflicts.
- Solution integrated with IMDB Api
- I have a much better async solution to discuss on skype call maybe. This one is sync due to :
 * Task mentioned that we need to return response to every request
 * I din't have to much time for this task ~ 4 hours, so was eager to use already backed solution from mongo
 * It would require Kafka integration
I'd be glad to explain how async rest should work, with integration to Kafka

### Running

When you have your SBT installed And MongoDb is running and configured in  < application.conf >
please in terminal open Project folder
and execute next command in command line:

```
sbt run

```

To stop application just press RETURN



Now you should be able to execute rest requests, while list examples of possible requests are:

To create new movie:
```
curl -X POST \
  http://0.0.0.0:9000/movie \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 528e1621-84f0-7835-e52a-d0a18879d871' \
  -d ' {
        "imdbId": "tt0111161",
        "availableSeats": 100,
        "screenId": "exampleCinema"
}   '
```

To book a movie ticket:
```
curl -X POST \
  http://0.0.0.0:9000/movie/book \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 5c915dfd-11e4-0e1f-303e-497427a773c1' \
  -d ' {
        "imdbId": "tt0111161",
        "screenId": "screen_123456"
 }'
```
To get a movie for ID's:
```
curl -X GET \
  'http://0.0.0.0:9000/movie?imdbId=tt0111161&screenId=screen_123456' \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: 0d9054f0-841f-0f35-8d9d-301ce792ae0b'
```

#### Notes

* In case of providing wrong id you will have corresponding warnings
* When there are no more available tickets you won't be able to book anymore

### Testing

In order to run unit testing please execute next command:

```
sbt test

```
