import akka.event.NoLogging
import akka.http.scaladsl.model.ContentTypes._
import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.model._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.stream.scaladsl.Flow
import com.mongodb.client.result.UpdateResult._
import org.mongodb.scala.result.UpdateResult
import org.scalatest.{FlatSpec, _}
import request.{BookMovieRequest, MovieRequest}
import responces.{FailureResponce, ImdbMovieResponce, MovieResponce, SucessResponce}
import routers.MovieRouter
import spray.json._
import traits.config.{Constants, Protocols}


object TestApplication extends MovieRouter { // Can be replaced with real DB if you have for test

  val addMovieRequest = MovieRequest("tt0111161", 100, "test")
  val addMovieRequestFail = MovieRequest("fail", 100, "test")
  val addMovieRequestAdded = MovieRequest("added", 100, "test")
  val bookMovieRequest = BookMovieRequest(addMovieRequest.imdbId, "test")
  val bookMovieRequestWrong = BookMovieRequest("wrong", "test")
  val movieResponse = MovieResponce(addMovieRequest.imdbId, addMovieRequest.screenId, "The Shawshank Redemption", 100, 0)


  override lazy val apiImdbConnectionFlow = Flow[HttpRequest].map { request =>
    if (request.uri.toString().endsWith(addMovieRequest.imdbId) || request.uri.toString().endsWith(addMovieRequestAdded.imdbId))
      HttpResponse(status = OK, entity = HttpEntity(ContentTypes.`application/json`, ImdbMovieResponce(addMovieRequest.imdbId, "The Shawshank Redemption", "10.0", "English").toJson.toString()))
    else
      HttpResponse(status = BadRequest, entity = HttpEntity(ContentTypes.`application/json`, "Unknown movie ID"))
  }

  override def addMovie(imdbId: String, screenId: String, title: String, availableSeats: Int): Either[UpdateResult, String] = {
    if (imdbId.equals(addMovieRequest.imdbId)) Left(acknowledged(12l, 0l, null)) else Right(s"Movie <$imdbId> at cinema <$screenId> already exists")
  }

  override def getMovie(imdbId: String, screenId: String): Either[MovieResponce, String] = {
    if (imdbId.equals(addMovieRequest.imdbId)) Left(movieResponse) else Right(s"Cant find movie <$imdbId> showing in cinema <$screenId>")
  }

  override def bookMovieTicket(imdbId: String, screenId: String): Either[UpdateResult, String] = {
    if(imdbId.equals(addMovieRequest.imdbId)) Left(acknowledged(12l, 1l, null)) else Right(s"Cant find movie <$imdbId> showing in cinema <$screenId>")
  }
}

/**
  * Created by andrew on 5/16/17.
  */
class ServiceSpec extends FlatSpec with Matchers with ScalatestRouteTest with Constants with Protocols {

  override def testConfigSource = "akka.loglevel = WARNING"
  val config = testConfig
  val logger = NoLogging
  val app = TestApplication
  val routes = app.routes

  "Movie Service" should "respond sucess to single POST query" in {

    Post(s"/movie", app.addMovieRequest) ~> routes ~> check {
      status shouldBe OK
      contentType shouldBe `application/json`
      responseAs[SucessResponce] shouldBe SucessResponce(success)
    }
  }

    it should "respond to single POST query with wrong movieID" in {
      Post(s"/movie", app.addMovieRequestFail) ~> routes ~> check {
        status shouldBe BadRequest
        contentType shouldBe `application/json`
        responseAs[FailureResponce] shouldBe FailureResponce(failed, "fail: incorrect ID")
      }
    }
    it should "respond to single POST query with added movieId" in {
      Post(s"/movie", app.addMovieRequestAdded) ~> routes ~> check {
        status shouldBe BadRequest
        contentType shouldBe `application/json`
        responseAs[FailureResponce] shouldBe FailureResponce(failed, s"Movie <${app.addMovieRequestAdded.imdbId}> at cinema <${app.addMovieRequestAdded.screenId}> already exists")
      }
    }

    it should "respond to single GET query" in {
      Get(s"/movie?${imdbIdKey}=${app.addMovieRequest.imdbId}&${screenIdKey}=${app.addMovieRequest.screenId}") ~> routes ~> check {
        status shouldBe OK
        contentType shouldBe `application/json`
        responseAs[MovieResponce] shouldBe app.movieResponse
      }
    }

    it should "respond to single GET query with wrong movieID" in {
      Get(s"/movie?${imdbIdKey}=fake&${screenIdKey}=${app.addMovieRequest.screenId}") ~> routes ~> check {
        status shouldBe BadRequest
        contentType shouldBe `application/json`
        responseAs[FailureResponce] shouldBe FailureResponce(failed, s"Cant find movie <fake> showing in cinema <${app.addMovieRequest.screenId}>")
      }
    }

    it should "respond to single POST update query" in {

      Post(s"/movie/book", app.bookMovieRequest) ~> routes ~> check {
        status shouldBe OK
        contentType shouldBe `application/json`
        responseAs[SucessResponce] shouldBe SucessResponce(success)
      }
    }

    it should "respond to single POST update query with wrong MovieId" in {
      Post(s"/movie/book", app.bookMovieRequestWrong) ~> routes ~> check {
        status shouldBe BadRequest
        contentType shouldBe `application/json`
        responseAs[FailureResponce] shouldBe FailureResponce(failed, s"Cant find movie <${app.bookMovieRequestWrong.imdbId}> showing in cinema <${app.bookMovieRequestWrong.screenId}>")
      }
    }

}
