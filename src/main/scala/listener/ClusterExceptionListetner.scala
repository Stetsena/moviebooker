package listener

import com.mongodb.event.{ClusterClosedEvent, ClusterDescriptionChangedEvent, ClusterListener, ClusterOpeningEvent}
import connection.MongoDbConnectionStatus

import scala.collection.JavaConverters._
/**
  * Created by andrew on 5/14/17.

  */


object ClusterExceptionListetner {
  def apply(status: MongoDbConnectionStatus): ClusterExceptionListetner = new ClusterExceptionListetner(status)
}

class ClusterExceptionListetner(status: MongoDbConnectionStatus) extends ClusterListener{
  override def clusterOpening(event: ClusterOpeningEvent): Unit  = {
    val i = 0
  }

  override def clusterClosed(event: ClusterClosedEvent): Unit  = {
    val i = 0
  }
  override def clusterDescriptionChanged(event: ClusterDescriptionChangedEvent): Unit  = {
    val exception = event.getNewDescription.getServerDescriptions.asScala.head.getException
    if( exception != null) {
      status.isConnected=false
    }
  }
}
