package listener

import com.mongodb.event.{CommandFailedEvent, CommandListener, CommandStartedEvent, CommandSucceededEvent}

/**
  * Created by andrew on 5/14/17.
  */
object ExceptionCommandListener{
  def apply(): ExceptionCommandListener = new ExceptionCommandListener()
}
class ExceptionCommandListener extends CommandListener {
  override def commandSucceeded(event: CommandSucceededEvent): Unit = {
    val i = 0
  }

  override def commandFailed(event: CommandFailedEvent): Unit = {
    val i = 0
  }

  override def commandStarted(event: CommandStartedEvent): Unit = {
    val i = 0
  }
}
