package routers

import akka.http.scaladsl.server.Directives._
import facades.MovieFacade
import request.{BookMovieRequest, MovieRequest}


/**
  * Created by andrew on 5/16/17.
  */
trait MovieRouter extends MovieFacade {

  val routes = {
    logRequestResult("akka-http-microservice") {
      pathPrefix("movie") {
        pathEnd {
          (get) {
            parameters('imdbId, 'screenId) { (imdbId, screenId) =>
              complete {
                getMovieInfo(imdbId, screenId)
              }
            }} ~
            (post & entity(as[MovieRequest])) { addMovieRequest =>
              complete {
                addMovieWithScreen(addMovieRequest)
              }
            }
        } ~
          path("book") {
            (post & entity(as[BookMovieRequest])) { bookMovieRequest =>
              complete {
                bookMovie(bookMovieRequest)
              }}
          }
      }
    }
  }

}
