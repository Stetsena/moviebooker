package connection

/**
  * Created by andrew on 5/14/17.
  */
object MongoDbConnectionStatus {
  def apply(): MongoDbConnectionStatus = new MongoDbConnectionStatus(true)
}
class MongoDbConnectionStatus(var isConnected: Boolean = true)
