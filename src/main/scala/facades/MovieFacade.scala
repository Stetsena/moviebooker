package facades

import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.model.StatusCodes._
import request.{BookMovieRequest, MovieRequest}
import responces.{FailureResponce, SucessResponce}
import spray.json._
import traits.db.MongoDb
import traits.services.ImdbService

/**
  * Created by andrew on 5/16/17.
  */
trait MovieFacade extends MongoDb with ImdbService {

  def getMovieInfo(imdbId: String, screenId: String): ToResponseMarshallable = {
    getMovie(imdbId, screenId) match {
      case Left(responce) => OK -> responce.toJson
      case Right(errorMessage) => BadRequest -> FailureResponce(failed, errorMessage).toJson
    }
  }

  def addMovieWithScreen(movie : MovieRequest): ToResponseMarshallable = {
    fetchImdbMovie(movie.imdbId).map[ToResponseMarshallable] {
      case Right(imdbMovie) => {
        addMovie(movie.imdbId, movie.screenId, imdbMovie.Title, movie.availableSeats) match {
          case Left(update) => OK -> SucessResponce(success).toJson
          case Right(errorMessage) => BadRequest -> FailureResponce(failed, errorMessage).toJson
        }
      }
      case Left(errorMessage) => BadRequest -> FailureResponce(failed, errorMessage).toJson
    }
  }

  def bookMovie(bookMovieRequest : BookMovieRequest): ToResponseMarshallable = {
    bookMovieTicket(bookMovieRequest.imdbId, bookMovieRequest.screenId) match {
      case Left(updated) => {
        if(updated.getModifiedCount>0)
          OK -> SucessResponce(success).toJson
        else BadRequest -> FailureResponce(failed, "All tickets Sold, Booking Unsuccessful").toJson
      }
      case Right(errorMessage) => BadRequest -> FailureResponce(failed, errorMessage).toJson
    }
  }
}