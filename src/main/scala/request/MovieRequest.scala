package request

import org.mongodb.scala.Document

/**
  * Created by andrew on 5/13/17.
  */
case class MovieRequest(imdbId: String, availableSeats : Int, screenId: String)

case class BookMovieRequest(imdbId: String, screenId: String)
