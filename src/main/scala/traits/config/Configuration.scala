package traits.config

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.stream.ActorMaterializer
import com.typesafe.config.ConfigFactory
import request.{BookMovieRequest, MovieRequest}
import responces.{FailureResponce, ImdbMovieResponce, MovieResponce, SucessResponce}
import spray.json.DefaultJsonProtocol

/**
  * Created by andrew on 5/16/17.
  */
trait Configuration extends Protocols with Constants{
  implicit val system = ActorSystem("microServiceSystem")
  implicit val materializer = ActorMaterializer()
  implicit val executor = system.dispatcher

  val config = ConfigFactory.load()
  val logger = Logging(system, "my.nice.string")
}


trait Protocols extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val imdbMovieFormat = jsonFormat4(ImdbMovieResponce.apply)
  implicit val movieFormat = jsonFormat5(MovieResponce.apply)
  implicit val addMovieRequestFormat = jsonFormat3(MovieRequest.apply)
  implicit val bookMovieFormat = jsonFormat2(BookMovieRequest.apply)
  implicit val sucessFormat = jsonFormat1(SucessResponce.apply)
  implicit val failFormat = jsonFormat2(FailureResponce.apply)
}

trait Constants {
  val imdbIdKey = "imdbId"
  val screenIdKey  = "screenId"
  val movieTitleKey  = "movieTitle"
  val availableSeatsKey  = "availableSeats"
  val reservedSeatsKey  = "reservedSeats"
  val reservedSeatsDefault  = 0
  val failed = "Failed"
  val success = "Success"
}