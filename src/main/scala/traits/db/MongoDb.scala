package traits.db

import java.util.concurrent.TimeUnit

import com.mongodb.client.model.UpdateOptions
import connection.MongoDbConnectionStatus
import listener.{ClusterExceptionListetner, ExceptionCommandListener}
import org.mongodb.scala.bson.BsonInt32
import org.mongodb.scala.connection.ClusterSettings
import org.mongodb.scala.model.Filters.{and, equal, lt}
import org.mongodb.scala.model.Updates.{combine, inc, setOnInsert}
import org.mongodb.scala.result.UpdateResult
import org.mongodb.scala.{Document, MongoClient, MongoClientSettings, MongoCollection, MongoDatabase, ServerAddress}
import responces.MovieResponce
import spray.json._
import traits.config.Configuration

import scala.collection.JavaConverters._
import scala.concurrent.Await
import scala.concurrent.duration.Duration
/**
  * Created by andrew on 5/16/17.
  */

trait MongoDb extends Configuration {

  val connection = MongoDbConnectionStatus()
  val clusterSettings: ClusterSettings = ClusterSettings.
    builder()
    .addClusterListener(ClusterExceptionListetner(connection))
    .hosts(List(new ServerAddress(config.getString("db.mongo.host"), config.getInt("db.mongo.port"))).asJava)
    .build()
  val settings: MongoClientSettings = MongoClientSettings
    .builder()
    .addCommandListener(ExceptionCommandListener())
    .clusterSettings(clusterSettings)
    .build()

  var mongoClient: MongoClient = MongoClient(settings)
  var database: MongoDatabase = mongoClient.getDatabase(config.getString("db.mongo.database"))
  var collection: MongoCollection[Document] = database.getCollection("screens")

  def addMovie(imdbId: String,screenId: String, title: String, availableSeats : Int ) : Either[UpdateResult, String] = {
    val inserted = Await.result(collection
      .updateOne
      (and
      (equal(imdbIdKey, imdbId),
        equal(screenIdKey, screenId)),
        combine
        (setOnInsert(imdbIdKey, imdbId),
          setOnInsert(screenIdKey, screenId),
          setOnInsert(movieTitleKey, title),
          setOnInsert(availableSeatsKey, availableSeats),
          setOnInsert(reservedSeatsKey, reservedSeatsDefault)),
        new UpdateOptions().upsert(true))
      .head(), Duration(5, TimeUnit.SECONDS))
    if(inserted.getMatchedCount==0) Left(inserted) else Right(s"Movie <$imdbId> at cinema <$screenId> already exists")
  }

  def getMovie(imdbId: String, screenId: String): Either[MovieResponce, String] = {
    val document = Await.result(collection.find(and(equal(imdbIdKey, imdbId), equal(screenIdKey, screenId))).first().head(), Duration(5, TimeUnit.SECONDS))
    if(document != null) Left(document.toJson().parseJson.convertTo[MovieResponce]) else Right(s"Cant find movie <$imdbId> showing in cinema <$screenId>")
  }

  def bookMovieTicket(imdbId: String, screenId: String): Either[UpdateResult,String] = {
    val cinemaMovie = Await.result(collection.find(and(equal(imdbIdKey, imdbId), equal(screenIdKey, screenId))).first().head(), Duration(5, TimeUnit.SECONDS))
    if(cinemaMovie == null) return Right(s"Cant find movie <$imdbId> showing in cinema <$screenId>")
    val limit: Int = cinemaMovie.get(availableSeatsKey).get.asInstanceOf[BsonInt32].intValue()
    Left(Await.result(collection
      .updateOne
      (and(equal(imdbIdKey, imdbId),
        equal(screenIdKey, screenId),
        lt(reservedSeatsKey, limit)
      ),
        inc(reservedSeatsKey, 1)).head(), Duration(5, TimeUnit.SECONDS)))
  }
}
