package traits.services

import java.io.IOException

import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.model.{HttpRequest, HttpResponse}
import akka.http.scaladsl.model.StatusCodes.{BadRequest, OK}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.scaladsl.{Flow, Sink, Source}
import responces.ImdbMovieResponce
import traits.config.{Configuration, Protocols}

import scala.concurrent.Future

/**
  * Created by andrew on 5/16/17.
  */
trait ImdbService extends Configuration {

  lazy val apiImdbConnectionFlow: Flow[HttpRequest, HttpResponse, Any] =
    Http().outgoingConnection(config.getString("services.ip-api.host"), config.getInt("services.ip-api.port"))

  def apiImdbRequest(request: HttpRequest): Future[HttpResponse] = Source.single(request).via(apiImdbConnectionFlow).runWith(Sink.head)

  def fetchImdbMovie(movieId: String): Future[Either[String, ImdbMovieResponce]] = {
    apiImdbRequest(RequestBuilding.Get(s"/?i=$movieId")).flatMap { response =>
      response.status match {
        case OK => Unmarshal(response.entity).to[ImdbMovieResponce].map(Right(_))
        case BadRequest => Future.successful(Left(s"$movieId: incorrect ID"))
        case _ => Unmarshal(response.entity).to[String].flatMap { entity =>
          val error = s"FreeGeoIP request failed with status code ${response.status} and entity $entity"
          logger.error(error)
          Future.failed(new IOException(error))
        }
      }
    }
  }

}