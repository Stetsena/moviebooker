import akka.http.scaladsl.Http
import routers.MovieRouter

import scala.io.StdIn


/**
  * Created by andrew on 5/13/17.
  */

object MicroserviceApp extends MovieRouter {

  def main(args: Array[String]) {
    if (connection.isConnected) {
      val bindingFuture = Http().bindAndHandle(routes, config.getString("http.interface"), config.getInt("http.port"))
      println(s"Server online at http://${config.getString("http.interface")}:${config.getInt("http.port")}")
      println("Press RETURN to stop...")
      StdIn.readLine()
      bindingFuture
        .flatMap(_.unbind())
        .onComplete(_ => {
          system.terminate()
          mongoClient.close()
        })
    } else {
      system.terminate()
      mongoClient.close()
    }
  }
}